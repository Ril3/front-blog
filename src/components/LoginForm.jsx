import { Form, Input, Button } from 'antd';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { loginWithCredentials } from '../stores/auth-slice';


export function LoginForm(){

    let history = useHistory();
    function handleClick() {
        history.push("/");
      }

    const dispatch = useDispatch();
    // const feedback = useSelector(state => state.auth.loginFeedback) 


    const onFinish = (values) => {
        dispatch(loginWithCredentials(values));
        handleClick()
    }

    return (
        <div className="forma">
            <h2>Login</h2>
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 5,
            }}
            onFinish={onFinish}
        >
            
            <Form.Item
                label="Email"
                name="email"
                rules={[
                    {
                        required: true,
                        message: 'Email is required',
                    },
                    {
                        type: 'email',
                        message: 'Please enter a valid email',
                    },
                ]}
            >
                <Input type="email" placeholder="Email"/>
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Password is required',
                    },
                ]}
            >
                <Input.Password  placeholder="Password"/>
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button className="forma-btn" type="primary" htmlType="submit">
                    Login
                </Button>
            </Form.Item>
        </Form>
        </div>
        
        
    )
}