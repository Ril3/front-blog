import '../css/PostList.css'
import { Button, Card, Col, Divider, Row } from "antd";
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { deletePost, fetchPosts } from '../stores/post-slice';

const { Meta } = Card;



export function ListPost() {

    const dispatch = useDispatch()
    const posts = useSelector(state => state.posts.list);

    useEffect(() => {
        dispatch(fetchPosts());
    }, [dispatch]);

    const onDelete = (id)=>{
        dispatch(deletePost(id))
       
    }

    return (
      <div className="list">
            <Divider orientation="center"><h2>Blogs</h2></Divider>
            <Row gutter={[80, 20]}>
            {posts.map(item=> 
                <Col key={item.id} xs={24} md={12}>
               
                    <Card
                         
                        post={item}
                        hoverable
                        // style={{ height: 400 }}
                        cover={<img alt="example" src={item.photo} />}
                        >
                    <Meta title={item.title} description={item.body} />
                    <Button onClick={()=> onDelete(item.id)} type="primary" htmlType="submit">
                         Delete
                    </Button>
                    </Card>
            </Col>)}
            </Row>
        </div>
  
    );
}

