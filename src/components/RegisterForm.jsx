
import { Form, Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../stores/auth-slice';


export function UserForm() {

    const dispatch = useDispatch();
    const feedback = useSelector(state => state.auth.registerFeedback);

    const onFinish = (values) => {
        dispatch(register(values));
    }

    return (
        <div className="forma">
            <h2>Register</h2>
            <Form
                className="forma"
                name="basic"
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 5,
                }}
                onFinish={onFinish}
            >
                {feedback && <p className="feedback">{feedback}</p>}
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Email is required',
                        },
                        {
                            type: 'email',
                            message: 'Please enter a valid email',
                        },
                    ]}
                >
                    <Input type="email" />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Password is required',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>



                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button className="forma-btn" type="primary" htmlType="submit">
                        Register
                    </Button>
                </Form.Item>
            </Form>
        </div>
        
    )
}