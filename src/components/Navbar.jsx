import React from "react";
import { Link } from "react-router-dom";
import "../css/Navbar.css";
import { useDispatch, useSelector } from "react-redux";



import { logout } from "../stores/auth-slice";

export default function Navbar(){


    const dispatch = useDispatch();
    const user = useSelector(state => state.auth.user);

        return (
            <nav className="navBar">
                <img className="logo" src="./images/logo.png" alt="Logo" />
                {!user ?
                            <ul>
                                <li>
                                    <Link className="link" to="/">Home</Link>
                                </li>
                                <li>
                                    <Link className="link" to="/register">Register</Link>
                                </li>
                                <li>
                                    <Link className="link" to="/login">Login</Link> 
                                </li>
                            </ul> 
                            :
                            <ul>
                                <li>
                                    <Link className="link" to="/">Home</Link>
                                </li>
                                <li>
                                    <Link className="link" to="/add">Write post</Link>
                                </li>
                                <li>
                                    <Link className="link" to="/login" onClick={() => dispatch(logout())}>Logout</Link>
                                </li>
                            </ul> 
            }

            </nav>
    )
}

