import { Form, Input, Button } from 'antd';
import '../css/AddPost.css';
// import { useState } from 'react';

const { TextArea } = Input;


export function AddPostForm({onFormSubmit}){

  
  function handleSubmit(value) {

   onFormSubmit(value)
}

    return (
        <div className="forma">
        <h2>Add Post</h2>

        
    <Form
        onFinish={handleSubmit}
        name="basic"
        labelCol={{
            span: 8,
        }}
        wrapperCol={{
            span: 8,
        }}
        
    >
        
        <Form.Item
            label="Title"
            name="title"
            rules={[
                {
                    required: true,
                    message: 'Title is required',
                },
            ]}
        >
            <Input type="text" />
        </Form.Item>

        <Form.Item
            label="Photo"
            name="photo"
            rules={[
                {
                    required: true,
                    message: 'photo is required',
                },
            ]}
        >
            <Input type="text" />
        </Form.Item>

        <Form.Item
            label="Text"
            name="body"
            rules={[
                {
                    required: true,
                    message: 'Text is required',
                },
            ]}
        >
            <TextArea rows={6} />
        </Form.Item>

        <Form.Item
            label="Date"
            name="date"
            rules={[
                {
                    required: true,
                    message: 'Date is required',
                },
            ]}
        >
            <Input type="date" />
        </Form.Item>

        <Form.Item
            wrapperCol={{
                offset: 8,
                span: 16,
            }}
        >
            <Button type="primary" htmlType="submit">
               Add post
            </Button>
        </Form.Item>
    </Form>
    </div>
    )
}