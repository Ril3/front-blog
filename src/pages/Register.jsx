import { UserForm } from "../components/RegisterForm";


export function Register(){
    return (
        <div className='registerPage'>
            <UserForm />
        </div>
    )
}