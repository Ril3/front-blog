// import axios from "axios";
// import { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { AddPostForm } from "../components/AddPostForm";
import { persistePost } from "../stores/post-slice";




export function AddPost(){

    const dispatch = useDispatch();


    // const [posts, setPosts] = useState([]);

    // async function addPost(onePost){
    //   const response = await axios.post('http://localhost:7000/api/post/add', onePost)
    //   setPosts([
    //       ...posts,
    //       response.data
    //   ]);
    //   alert("You've successfully added new operation")
    // }
    let history = useHistory();
    function handleClick() {
        history.push("/");
      }


    const addPost= (values)=>{
        dispatch(persistePost(values))
        handleClick()
    }
    return (
        <div>
            <AddPostForm onFormSubmit={addPost} />
        </div>
    )
}