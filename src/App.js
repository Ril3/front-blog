import { Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Register } from "./pages/Register";
import "./css/Navbar.css"
import { ProtectedRoute } from "./components/ProtectedRoute";
import { AddPost } from "./pages/AddPost";
import { useEffect } from "react";
import { loginWithToken } from "./stores/auth-slice";
import { useDispatch } from "react-redux";


function App() {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loginWithToken());
  }, [dispatch]);


  return (
    <div className="mainScreen">
      <Navbar />
      <Switch>
          <Route path="/register">
            <Register />
          </Route>

          <Route path="/login">
            <Login />
          </Route>

          <Route path="/" exact>
            <Home />

          </Route>

          <ProtectedRoute path="/add">
            <AddPost />
          </ProtectedRoute>

        </Switch>
    </div>
  );
}

export default App;
