import 'antd/dist/antd.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import './services/token-interceptor'
import { Provider } from 'react-redux';
import { store } from './stores/store';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <App/>
        </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

