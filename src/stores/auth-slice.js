import { createSlice } from "@reduxjs/toolkit";
import { AuthService } from "../services/AuthService";
import { addUser } from "./user-slice";


const authSlice = createSlice({
    name: "auth",
    initialState:{
        user: false,
        loginFeedback: "",
        registerFeedback:""
    },
    reducers: {
        login(state, {payload}){
            state.user = payload;
        },
        logout(state, {payload}){
            state.user = null;
            localStorage.removeItem("token")
        },
        updateLoginFeedback(state, {payload}){
            state.loginFeedback = payload;
        },
        updateRegisterFeedback(state, {payload}){
            state.registerFeedback = payload
        }
    }
})

export const {login, logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;
export default authSlice.reducer


export const loginWithToken = () => async (dispatch) => {

    const token = localStorage.getItem("token");

    if(token){
        try {
            const user = await AuthService.fetchAccount();
            dispatch(login(user))
        } catch (error) {
            dispatch(logout())
            console.log(error)
        }
    }else{
        dispatch(logout())
    }
}


export const register = (body) => async (dispatch) => {
    try {
        const {user, token} = await AuthService.register(body);
        localStorage.setItem("token", token);

        dispatch(updateRegisterFeedback("Registration have been successfull"));
        dispatch(addUser(user));
        dispatch(login(user));
    } catch (error) {
        dispatch(updateRegisterFeedback(error.response.data.error));
    }
}

export const loginWithCredentials = (credentials) => async (dispatch) => {
    try {
        const user = await AuthService.login(credentials);
        dispatch(updateLoginFeedback("Login have been successfull"));
        dispatch(login(user));
    } catch (error) {
        dispatch(updateLoginFeedback("Email and/or password incorrect"))
    }
}