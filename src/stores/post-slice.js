import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const postSlice = createSlice({

    name : 'posts',
    initialState : {
        list : []
    },
    reducers: {
        setList(state, {payload}){
            state.list = payload
        },
        addPost(state, {payload}){
            state.list = [...state.list, payload]
        },
        // deletePost(state, {payload}){
        //     state.list.filter(item => item.id !== payload)
        // }
    } 
});

export const { setList, addPost} = postSlice.actions;
export default postSlice.reducer

export const fetchPosts = () => async (dispatch) =>{
    const response = await axios.get('https://blog-project-back.herokuapp.com/api/post/all')
    dispatch(setList(response.data))
}


export const persistePost =(onePost) => async (dispatch)=>{
    const respons= await axios.post('https://blog-project-back.herokuapp.com/api/post/add', onePost);
    dispatch(addPost(respons.data))
}

export const deletePost = (id) => async (dispatch)=>{
    await axios.delete("https://blog-project-back.herokuapp.com/api/post/delete/" + id);
    dispatch(fetchPosts())
}