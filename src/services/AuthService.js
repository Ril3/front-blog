import axios from "axios";


export class AuthService {
    
    static async register(user){
        const response = await axios.post('https://blog-project-back.herokuapp.com/api/user/register', user);
        return response.data
    }

    static async login(credentials){
        const response = await axios.post('https://blog-project-back.herokuapp.com/api/user/login', credentials);

        localStorage.setItem("token", response.data.token)
        return response.data.user
    }

    static async fetchAccount(){
        const response = await axios.get('https://blog-project-back.herokuapp.com/api/user/account');
        return response.data
    }
}