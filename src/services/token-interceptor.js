import axios from "axios";
// import { logout } from "../stores/auth-slice";
// import {store} from '../stores/store'
/**
 * Les interceptors de axios permettent, comme leur nom l'indique, d'intercepter
 * toutes les requêtes (ou responses) faites avec axios afin de pouvoir les modifier
 * ou déclencher une action ou autre avant d'exécuter les requêtes en question.
 * 
 * Ici, on s'en sert afin d'ajouter le token dans les headers (si celui ci existe) de
 * toutes les requêtes qu'on fera, ça nous évite de devoir le faire pour chaque requête
 * axios
 */
axios.interceptors.request.use((config) => {
    const token =localStorage.getItem('token');
    if(token) {

        config.headers.authorization = 'bearer '+token;
    }
    return config;
});

// axios.interceptors.response.use((response) => {
//     return response;
// }, (error) => {
        
//     if(error.response.status === 401) {
//         store.dispatch(logout());
//     }
// })