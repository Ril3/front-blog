# For Back-End I've used:

- NodeJS
- Express server
- MySQL
- Babel
- Rooter/Controler
- Dotenv-flow

# For Front-End I've used:

- React-Redux-toolkit
- React router
- Axios
- CSS
- Ant-design

 # Deployment:
    Back-end of the project is deployed in Heroku: https://blog-project-back.herokuapp.com
    Front-end of this project is deplyed on Netlify on this URL(This is the homepage of the app aswell): https://ril3.gitlab.io/front-blog


